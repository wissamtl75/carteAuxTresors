Carte aux Trésors
Un simulateur simple pour explorer une carte à la recherche de trésors.

Description
Ce projet simule les déplacements d'aventuriers sur une carte où ils peuvent trouver des trésors tout en évitant les obstacles comme les montagnes. Les aventuriers peuvent pivoter et avancer dans une direction spécifique, et lorsqu'ils trouvent un trésor, ils le collectent.

Fonctionnalités
Charger une carte à partir d'un fichier texte.
Placer des montagnes et des trésors sur la carte.
Simuler le mouvement d'aventuriers à la recherche de trésors.
Afficher le résultat final après que tous les aventuriers aient terminé leurs mouvements.
Comment l'utiliser
Assurez-vous d'avoir tous les fichiers nécessaires et compilez le projet.
Lancez l'application.
Fournissez le chemin d'accès au fichier de carte.
Regardez le résultat à la console !
Format du fichier de carte
Votre fichier doit suivre un format spécifique pour que la simulation fonctionne correctement. Voici un exemple :

# Environnement technique
Langage: Java 17 ou supérieure
Framework: Lombok 
Gestion de dépendances: maven

# Format de la carte
C - 3 - 4

# Montagnes
M - 1 - 0
M - 2 - 1

# Trésors (avec quantités)
T - 0 - 3 - 2
T - 1 - 3 - 3

# Format des aventuriers
# Nom - X - Y - Orientation - Séquence de mouvements
A - Lara - 1 - 1 - S - AADADAGGA
A - Indiana - 0 - 3 - N - AGAGAGAGA