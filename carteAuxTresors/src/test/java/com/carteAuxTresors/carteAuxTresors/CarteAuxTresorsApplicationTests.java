package com.carteAuxTresors.carteAuxTresors;

import com.carteAuxTresors.carteAuxTresors.mesObjets.Carte;
import com.carteAuxTresors.carteAuxTresors.mesObjets.aventurier.Aventurier;
import com.carteAuxTresors.carteAuxTresors.mesObjets.common.Position;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CarteAuxTresorsApplicationTests {


	private Aventurier aventurier;

	@BeforeEach
	void setUp() {
		aventurier = new Aventurier("Lara", 1, 1, "S", "AADADAGGA");
	}//

	@Test
	void testAventurierInitialization() {
		assertEquals("Lara", aventurier.getNom());
		assertEquals("S", aventurier.getOrientation());
		assertEquals(0, aventurier.getTresorsCollectes());
	}

	@Test
	void testAventurierMove() {
		Carte carte = new Carte(3, 4);
		aventurier.move(carte);
		Position expectedPosition = new Position(0, 3);
		assertEquals(expectedPosition, aventurier.getPosition());
	}

}
