package com.carteAuxTresors.carteAuxTresors.GestionDeLaCarte;

import com.carteAuxTresors.carteAuxTresors.mesObjets.Carte;
import com.carteAuxTresors.carteAuxTresors.mesObjets.Montagne;
import com.carteAuxTresors.carteAuxTresors.mesObjets.Tresor;
import com.carteAuxTresors.carteAuxTresors.mesObjets.aventurier.Aventurier;
import com.carteAuxTresors.carteAuxTresors.mesObjets.common.Position;

import java.util.List;

public class Afficheur {
    public static void afficherResultat(Carte carte, List<Aventurier> aventuriers) {
        for (int y = 0; y < carte.getHauteur(); y++) {
            for (int x = 0; x < carte.getLargeur(); x++) {
                Position position = new Position(x, y);
                Object obj = carte.obtenirElement(position.getX(), position.getY());
                if (obj instanceof Montagne) {
                    System.out.print("M ");
                } else if (obj instanceof Tresor) {
                    System.out.print("T(" + ((Tresor) obj).getQuantite() + ") ");
                } else if (obj instanceof Aventurier) {
                    System.out.print(((Aventurier) obj).getDirection().name().charAt(0) + " ");
                } else {
                    System.out.print(". ");
                }
            }
            System.out.println();
        }

        System.out.println("\nAventuriers:");
        for (Aventurier aventurier : aventuriers) {
            System.out.println(aventurier.getNom() + " est à la position (" +
                    aventurier.getPosition().getX() + ", " + aventurier.getPosition().getY() +
                    ") orienté vers " + aventurier.getDirection() + " avec " +
                    aventurier.getTresorsCollectes() + " trésors ramassés.");
        }
    }
}
