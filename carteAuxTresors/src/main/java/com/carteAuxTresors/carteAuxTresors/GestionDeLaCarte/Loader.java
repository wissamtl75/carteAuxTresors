package com.carteAuxTresors.carteAuxTresors.GestionDeLaCarte;

import com.carteAuxTresors.carteAuxTresors.mesObjets.Carte;
import com.carteAuxTresors.carteAuxTresors.mesObjets.Montagne;
import com.carteAuxTresors.carteAuxTresors.mesObjets.Tresor;
import com.carteAuxTresors.carteAuxTresors.mesObjets.aventurier.Aventurier;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Loader {

    public static Carte chargerCarteDepuisFichier(String cheminFichier) throws IOException {
        Carte carte = null;

        try (BufferedReader br = new BufferedReader(new FileReader(cheminFichier, StandardCharsets.UTF_8))) {
            String ligne;
            while ((ligne = br.readLine()) != null) {
                if (ligne.trim().startsWith("#")) {
                    continue;
                }

                String[] parties = ligne.split(" - ");
                switch (parties[0]) {
                    case "C":
                        int largeur = Integer.parseInt(parties[1]);
                        int hauteur = Integer.parseInt(parties[2]);
                        carte = new Carte(largeur, hauteur);
                        break;
                    case "M":
                        Montagne montagne = new Montagne(Integer.parseInt(parties[1]), Integer.parseInt(parties[2]));
                        carte.ajouterElement(montagne, montagne.getPosition().getX(), montagne.getPosition().getY());
                        break;
                    case "T":
                        Tresor tresor = new Tresor(Integer.parseInt(parties[1]), Integer.parseInt(parties[2]), Integer.parseInt(parties[3]));
                        carte.ajouterElement(tresor, tresor.getPosition().getX(), tresor.getPosition().getY());
                        break;
                }
            }
        }

        return carte;
    }

    public static List<Aventurier> chargerAventuriersDepuisFichier(String cheminFichier) throws IOException {
        List<Aventurier> aventuriers = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(cheminFichier))) {
            String ligne;
            while ((ligne = br.readLine()) != null) {
                if (ligne.trim().startsWith("#")) {
                    continue;
                }

                String[] parties = ligne.split(" - ");
                if ("A".equals(parties[0])) {
                    Aventurier aventurier = new Aventurier(parties[1], Integer.parseInt(parties[2]), Integer.parseInt(parties[3]), parties[4], parties[5]);
                    aventuriers.add(aventurier);
                }
            }
        }

        return aventuriers;
    }
}
