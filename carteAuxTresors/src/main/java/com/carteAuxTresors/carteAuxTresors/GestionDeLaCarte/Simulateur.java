package com.carteAuxTresors.carteAuxTresors.GestionDeLaCarte;

import com.carteAuxTresors.carteAuxTresors.mesObjets.Carte;
import com.carteAuxTresors.carteAuxTresors.mesObjets.aventurier.Aventurier;

import java.util.List;

public class Simulateur {
    public void executerSimulation(Carte carte, List<Aventurier> aventuriers) {
        for (Aventurier aventurier : aventuriers) {
            aventurier.move(carte);
        }
    }
}

