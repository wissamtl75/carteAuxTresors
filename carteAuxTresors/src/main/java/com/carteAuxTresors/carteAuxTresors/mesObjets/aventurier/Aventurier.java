package com.carteAuxTresors.carteAuxTresors.mesObjets.aventurier;

import com.carteAuxTresors.carteAuxTresors.mesObjets.Carte;
import com.carteAuxTresors.carteAuxTresors.mesObjets.Montagne;
import com.carteAuxTresors.carteAuxTresors.mesObjets.common.Position;
import lombok.Data;

@Data
public class Aventurier {

    private Position position;
    private String nom;
    private String orientation;
    private String mouvements;

    private Direction direction;

    private int tresorsCollectes = 0;

    public Aventurier(String nom, int x, int y, String orientation, String mouvements) {
        this.position = new Position(x, y);
        this.nom = nom;
        this.orientation = orientation;
        this.mouvements = mouvements;
        this.direction = Direction.fromString(orientation);
    }

    public void move(Carte carte) {
        for (char action : mouvements.toCharArray()) {
            switch (action) {
                case 'A':
                    avancer(carte);
                    break;
                case 'G':
                    this.direction = direction.pivotGauche();
                    break;
                case 'D':
                    this.direction = direction.pivotDroite();
                    break;
            }
        }
    }

    public void avancer(Carte carte) {
        Position nextPos = position.copy();
        switch (direction) {
            case N:
                nextPos.setY(nextPos.getY() - 1);
                break;
            case E:
                nextPos.setX(nextPos.getX() + 1);
                break;
            case S:
                nextPos.setY(nextPos.getY() + 1);
                break;
            case W:
                nextPos.setX(nextPos.getX() - 1);
                break;
        }


        Object contenuCase = carte.obtenirElement(nextPos.getX(), nextPos.getY());
        if (!(contenuCase instanceof Montagne) && withinBoundaries(carte, nextPos)) {
            position = nextPos;
        }
    }

    private boolean withinBoundaries(Carte carte, Position position) {
        return position.getX() >= 0 && position.getX() < carte.getLargeur() &&
                position.getY() >= 0 && position.getY() < carte.getHauteur();
    }

}