package com.carteAuxTresors.carteAuxTresors.mesObjets;

import com.carteAuxTresors.carteAuxTresors.mesObjets.common.Position;
import lombok.Data;

import java.util.*;

@Data
public class Carte {
    private int largeur;
    private int hauteur;
    private Map<Position, Object> grille = new HashMap<>();

    public Carte(int largeur, int hauteur) {
        this.largeur = largeur;
        this.hauteur = hauteur;
    }

    public void ajouterElement(Object obj, int x, int y) {
        grille.put(new Position(x, y), obj);
    }

    public Object obtenirElement(int x, int y) {
        return grille.get(new Position(x, y));
    }
}


