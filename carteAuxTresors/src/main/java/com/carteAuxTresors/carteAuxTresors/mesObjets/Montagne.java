package com.carteAuxTresors.carteAuxTresors.mesObjets;

import com.carteAuxTresors.carteAuxTresors.mesObjets.common.Position;
import lombok.Data;

@Data
public class Montagne{
    private Position position;

    public Montagne(int x, int y) {
        this.position = new Position(x, y);
    }
}
