package com.carteAuxTresors.carteAuxTresors.mesObjets.aventurier;

public enum Direction {
    N, E, S, W;

    public Direction pivotGauche() {
        switch (this) {
            case N: return W;
            case W: return S;
            case S: return E;
            case E: return N;
            default: throw new IllegalArgumentException();
        }
    }

    public Direction pivotDroite() {
        switch (this) {
            case N: return E;
            case E: return S;
            case S: return W;
            case W: return N;
            default: throw new IllegalArgumentException();
        }
    }

    public static Direction fromString(String orientation) {
        switch (orientation) {
            case "N": return N;
            case "E": return E;
            case "S": return S;
            case "W": return W;
            default: throw new IllegalArgumentException("Orientation non valide: " + orientation);
        }
    }
}

