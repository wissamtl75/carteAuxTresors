package com.carteAuxTresors.carteAuxTresors.mesObjets;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Case {
    private int x;
    private int y;

    public Case(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
