package com.carteAuxTresors.carteAuxTresors.mesObjets;

import com.carteAuxTresors.carteAuxTresors.mesObjets.common.Position;
import lombok.Data;

@Data
public class Tresor {

    private Position position;
    private int quantite;

    public Tresor(int x, int y, int quantite) {
        this.position = new Position(x, y);
        this.quantite = quantite;
    }
}
