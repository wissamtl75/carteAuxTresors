package com.carteAuxTresors.carteAuxTresors;

import com.carteAuxTresors.carteAuxTresors.GestionDeLaCarte.Afficheur;
import com.carteAuxTresors.carteAuxTresors.GestionDeLaCarte.Loader;
import com.carteAuxTresors.carteAuxTresors.GestionDeLaCarte.Simulateur;
import com.carteAuxTresors.carteAuxTresors.mesObjets.Carte;
import com.carteAuxTresors.carteAuxTresors.mesObjets.aventurier.Aventurier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.IOException;
import java.util.List;

@SpringBootApplication
public class CarteAuxTresorsApplication {
	public static void main(String[] args) throws IOException {
		try {
		SpringApplication.run(CarteAuxTresorsApplication.class, args);

		Carte carte = Loader.chargerCarteDepuisFichier("C:\\Users\\wissam.tlati\\Desktop\\carteAuxTresors\\carteAuxTresors\\src\\main\\resources\\Carte.txt");
		List<Aventurier> aventuriers = Loader.chargerAventuriersDepuisFichier("C:\\Users\\wissam.tlati\\Desktop\\carteAuxTresors\\carteAuxTresors\\src\\main\\resources\\Carte.txt");
		Simulateur simulation = new Simulateur();

		simulation.executerSimulation(carte, aventuriers);

		Afficheur.afficherResultat(carte, aventuriers);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erreur lors du chargement des fichiers.");
		}
	}
}
